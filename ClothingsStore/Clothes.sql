USE [master]
GO
/****** Object:  Database [Clothes]    Script Date: 31/08/2017 09:17:13 ******/
CREATE DATABASE [Clothes]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Clothes', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Clothes.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Clothes_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Clothes_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Clothes] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Clothes].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Clothes] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Clothes] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Clothes] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Clothes] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Clothes] SET ARITHABORT OFF 
GO
ALTER DATABASE [Clothes] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Clothes] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Clothes] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Clothes] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Clothes] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Clothes] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Clothes] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Clothes] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Clothes] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Clothes] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Clothes] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Clothes] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Clothes] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Clothes] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Clothes] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Clothes] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Clothes] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Clothes] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Clothes] SET  MULTI_USER 
GO
ALTER DATABASE [Clothes] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Clothes] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Clothes] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Clothes] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Clothes] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Clothes]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 31/08/2017 09:17:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[AdminID] [int] IDENTITY(1,1) NOT NULL,
	[AdminName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Categories]    Script Date: 31/08/2017 09:17:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Clothes]    Script Date: 31/08/2017 09:17:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clothes](
	[ClothID] [int] IDENTITY(1,1) NOT NULL,
	[ClothCategory] [int] NOT NULL,
	[ClothType] [int] NOT NULL,
	[ClothCompany] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[Discount] [real] NULL,
	[Picture] [nvarchar](50) NULL,
 CONSTRAINT [PK_Clothes] PRIMARY KEY CLUSTERED 
(
	[ClothID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Companies]    Script Date: 31/08/2017 09:17:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Companies](
	[CompanyID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Types]    Script Date: 31/08/2017 09:17:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Types](
	[TypeId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Types] PRIMARY KEY CLUSTERED 
(
	[TypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([AdminID], [AdminName], [Password]) VALUES (1, N'michal', N'1234')
INSERT [dbo].[Admin] ([AdminID], [AdminName], [Password]) VALUES (2, N'ayala', N'2222')
SET IDENTITY_INSERT [dbo].[Admin] OFF
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (1, N'men')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (2, N'women')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (3, N'kids')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (4, N'baby')
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[Clothes] ON 

INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (32, 3, 2, 2, 120.0000, 21, N'1a0d215b-3f20-43b5-8c64-fe2cf8dd0398.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (33, 2, 1, 1, 90.0000, 12, N'b469ae7f-a8af-4483-985d-9c411b4e327c.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (35, 2, 1, 3, 455.0000, 32, N'd4feddab-45d0-491a-9a51-57499b98d7e4.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (38, 1, 6, 1, 100.0000, 11, N'f3460943-d3ef-4a81-a6f0-c1963250306e.png')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (39, 1, 3, 1, 120.0000, 22, N'cb4b7816-cea1-432b-9070-58878cbaafae.png')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (40, 1, 3, 2, 190.0000, 22, N'b4ca03e6-b159-43c0-872a-4540c5ec20f5.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (41, 3, 1, 2, 120.0000, 5, N'd7f0159a-be43-46bd-a140-87b0e35f386f.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (42, 4, 9, 4, 90.0000, 12, N'848be6e8-1e7e-4ac5-853e-421c7b259693.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (43, 2, 1, 1, 300.0000, 21, N'e84c6bb2-0a6c-43ab-a031-a036d5e4096b.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (44, 2, 1, 2, 200.0000, 13, N'516f4166-beb3-464f-872e-77cf3ec0116b.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (46, 3, 8, 2, 90.0000, 11, N'1ba63a2c-246e-4cc5-896a-c443b5ba2420.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (47, 4, 9, 1, 80.0000, 11, N'72bd89af-9fe6-46c0-a84e-1f2c5fd42a6d.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (48, 3, 5, 1, 60.0000, 5, N'8e4a26c5-75e6-4f9f-a80d-78ad33e2074f.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (49, 1, 6, 2, 150.0000, 10, N'8aed4204-6e60-4867-ba48-cd296ba57ef2.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (50, 1, 3, 2, 240.0000, 12, N'0188aed2-b78c-4d35-8119-ffed6f4e50c0.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (51, 2, 7, 2, 300.0000, 20, N'760128f2-a23c-435f-a2d7-a0ba01806854.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (52, 2, 1, 3, 560.0000, 10, N'55621603-5a58-4510-9344-8bd50a561bd9.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (53, 3, 7, 4, 70.0000, 7, N'8276a931-3f7e-4ae9-8062-f17cb7e103f3.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (54, 4, 7, 2, 60.0000, 6, N'ec5b36b1-e9f2-4e3a-aabc-5ed12746cd21.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (55, 4, 1, 1, 90.0000, 9, N'15c89191-6c03-4aa6-9723-81dcdeab017b.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (56, 3, 3, 1, 60.0000, 6, N'7804dad9-18b4-44d4-b9c7-12e159285b95.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (57, 4, 8, 4, 130.0000, 12, N'e2dbfd58-d314-4b10-a264-17088519132c.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (58, 2, 7, 2, 190.0000, 9, N'a29c3e19-e636-4f44-a942-243b35329270.png')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (59, 1, 4, 2, 600.0000, 20, N'9c75ac6e-32bc-4d92-bac9-f9de641bb1fe.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (60, 4, 7, 2, 90.0000, 5, N'437ebb4f-d9fe-4fb2-87d0-87c60b29249a.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (61, 3, 4, 2, 210.0000, 10, N'26e74364-386a-4afc-9bd2-e61a29376ac7.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (62, 3, 3, 1, 99.0000, 9, N'b84e7d80-1f1a-4972-bc81-c1176b16d6ab.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (63, 1, 7, 1, 90.0000, 9, N'e6d4d3b5-7899-47a1-9c10-67a979a621fa.png')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (64, 1, 3, 2, 190.0000, 9, N'6f4208cb-2812-4003-bed9-57da5fe88620.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (65, 3, 1, 2, 90.0000, 9, N'5972dd0b-ec7e-4d91-b73e-bcaf033af385.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (66, 4, 7, 2, 90.0000, 9, N'18bcfabf-ba91-4fda-8556-c55e84c3bc4a.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (67, 2, 7, 2, 190.0000, 9, N'a22c95c9-ab79-4007-884b-af065e66f950.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (68, 2, 7, 4, 120.0000, 12, N'609b2250-20ca-4c6c-98b7-91b64858ac31.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (69, 4, 1, 4, 90.0000, 9, N'6cfdfac3-b2d9-411e-80eb-ef71e30d6de2.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (70, 4, 9, 1, 80.0000, 8, N'a0b087dd-4ae7-43b8-a787-8d203ed44f88.jpg')
INSERT [dbo].[Clothes] ([ClothID], [ClothCategory], [ClothType], [ClothCompany], [Price], [Discount], [Picture]) VALUES (71, 1, 3, 2, 180.0000, 8, N'c6631398-9e85-4165-939e-bdc54a9345fc.jpg')
SET IDENTITY_INSERT [dbo].[Clothes] OFF
SET IDENTITY_INSERT [dbo].[Companies] ON 

INSERT [dbo].[Companies] ([CompanyID], [CompanyName]) VALUES (1, N'castro')
INSERT [dbo].[Companies] ([CompanyID], [CompanyName]) VALUES (2, N'zara')
INSERT [dbo].[Companies] ([CompanyID], [CompanyName]) VALUES (3, N'mekimi')
INSERT [dbo].[Companies] ([CompanyID], [CompanyName]) VALUES (4, N'mango')
SET IDENTITY_INSERT [dbo].[Companies] OFF
SET IDENTITY_INSERT [dbo].[Types] ON 

INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (1, N'dress')
INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (2, N'skirt')
INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (3, N'blouse')
INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (4, N'suit')
INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (5, N'tight')
INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (6, N'belt')
INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (7, N'shirt')
INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (8, N'coat')
INSERT [dbo].[Types] ([TypeId], [TypeName]) VALUES (9, N'hat')
SET IDENTITY_INSERT [dbo].[Types] OFF
ALTER TABLE [dbo].[Clothes]  WITH CHECK ADD  CONSTRAINT [FK_Clothes_Categories] FOREIGN KEY([ClothCategory])
REFERENCES [dbo].[Categories] ([CategoryID])
GO
ALTER TABLE [dbo].[Clothes] CHECK CONSTRAINT [FK_Clothes_Categories]
GO
ALTER TABLE [dbo].[Clothes]  WITH CHECK ADD  CONSTRAINT [FK_Clothes_Companies] FOREIGN KEY([ClothCompany])
REFERENCES [dbo].[Companies] ([CompanyID])
GO
ALTER TABLE [dbo].[Clothes] CHECK CONSTRAINT [FK_Clothes_Companies]
GO
ALTER TABLE [dbo].[Clothes]  WITH CHECK ADD  CONSTRAINT [FK_Clothes_Types] FOREIGN KEY([ClothType])
REFERENCES [dbo].[Types] ([TypeId])
GO
ALTER TABLE [dbo].[Clothes] CHECK CONSTRAINT [FK_Clothes_Types]
GO
USE [master]
GO
ALTER DATABASE [Clothes] SET  READ_WRITE 
GO

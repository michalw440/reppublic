﻿using System.Collections.Generic;
using System.Linq;

namespace Seldat
{
    public class ClothTypeLogic : BaseLogic
    {
        public List<ClothTypeModel> GetTypesByCategory(int categoryID)
        {
            return DB.Categories.Where(c =>c.CategoryID==categoryID).FirstOrDefault().Types
                .Select(t => new ClothTypeModel { id = t.TypeId, descripition = t.TypeName }).ToList();
        }
    }
}

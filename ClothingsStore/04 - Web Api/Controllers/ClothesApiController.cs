﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat.Controllers
{
    [EnableCors("*", "*", "*")]
    public class ClothesApiController : ApiController
    {
        private ClothesLogic logic = new ClothesLogic();

        [HttpGet]
        [Route("api/clothes/{id}")]
        public HttpResponseMessage GetClothByCategory(int id)
        {
            try
            {
                List<ClothModel> clothes = logic.GetClothByCategory(id);
                return Request.CreateResponse(HttpStatusCode.OK, clothes);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(
                    HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        [HttpGet]
        [Route("api/oneCloth/{id}")]
        public HttpResponseMessage GetOneCloth([FromUri]int id)
        {
            try
            {
                ClothModel cloth = logic.GetOneCloth(id);
                return Request.CreateResponse(HttpStatusCode.OK, cloth);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(
                    HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        [HttpPut]
        [Route("api/clothes/{id}")]
        public HttpResponseMessage UpdateCloth([FromUri]int id, [FromBody]ClothModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.GetErrors());
                model.id = id;
                ClothModel cloth  = logic.UpdateFullCloth(model);
                return Request.CreateResponse(HttpStatusCode.OK, cloth);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(
                    HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        [HttpDelete]
        [Route("api/clothes/{id}")]
        public HttpResponseMessage DeleteCloth(int id)
        {
            try
            {
                string fullPath = HttpContext.Current.Server.MapPath("~/Images/" + logic.GetFileName(id));
                if (File.Exists(fullPath))
                {
                    File.Delete(fullPath);
                }
                logic.DeleteCloth(id);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse
                    (HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        [HttpPost]
        [Route("api/clothes")]
        public HttpResponseMessage AddCloth([FromBody]ClothModel model)
        {
            try
            {
               if (!ModelState.IsValid)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.GetErrors());
                model = logic.AddCloth(model);
                return Request.CreateResponse(HttpStatusCode.Created, model);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        [HttpPost]
        [Route("api/upload")]
        public HttpResponseMessage UploadFile()
        {
            int id = Convert.ToInt32(HttpContext.Current.Request.Form["id"]);
            uploadFileHelp(id);
            return Request.CreateResponse(HttpStatusCode.Created, new { });
        }

        [HttpPost]
        [Route("api/update")]
        public HttpResponseMessage UpdateFile()
        {
            int id = Convert.ToInt32(HttpContext.Current.Request.Form["id"]);
            string fullPath = HttpContext.Current.Server.MapPath("~/Images/" + logic.GetFileName(id));
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }
            uploadFileHelp(id);
            return Request.CreateResponse(HttpStatusCode.OK, new { });
        }

        public void uploadFileHelp(int id)
        {
            if (HttpContext.Current.Request.Files.Count > 0)
            {
                string originalName = HttpContext.Current.Request.Files[0].FileName;
                string newFileNme = Guid.NewGuid().ToString() + Path.GetExtension(originalName);
                string fullPathAndFilename = HttpContext.Current.Server.MapPath("~/Images/" + newFileNme);
                //save file 
                HttpContext.Current.Request.Files[0].SaveAs(fullPathAndFilename);
                logic.UploadFile(id, newFileNme);
            }
            string fileName = Guid.NewGuid().ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                logic.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}




﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Seldat
{
    [EnableCors("*", "*", "*")]
    public class TypeApiController : ApiController
    {
        private ClothTypeLogic logic = new ClothTypeLogic();
        [HttpGet]
        [Route("api/getTypes/category/{id}")]
        public HttpResponseMessage GetTypes([FromUri]int id)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.GetErrors());
                List<ClothTypeModel> companies = logic.GetTypesByCategory(id);
                return Request.CreateResponse(HttpStatusCode.Created, companies);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.GetUserFriendlyMessage());
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                logic.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

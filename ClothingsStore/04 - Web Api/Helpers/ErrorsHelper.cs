﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.ModelBinding;

namespace Seldat
{
    public static class ErrorsHelper
    {
        //extension
        public static string GetMostInnerMessage(this Exception ex)
        {
            if (ex.InnerException == null)
                return ex.Message;
            return GetMostInnerMessage(ex.InnerException);
        }

        public static string GetUserFriendlyMessage(this Exception ex)
        {            
            string msg = ex.GetMostInnerMessage();
            if (msg.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
            {
                return "You cant delete an item which connected to other item";
            }
            else if (msg.Contains("No connection string named "))
                return "Your connection string name not found";
            else if (msg.Contains("Validation failed for one or more entities"))
                return "You didint send an object to add";
            return "There was a prbolem";
        }

        public static string GetOneError(this ModelStateDictionary modelState)
        {
            return modelState.Where(ms => ms.Value.Errors.Any())
                       .Select(ms => ms.Value.Errors[0].ErrorMessage).FirstOrDefault();
        }

        public static Dictionary<string, List<string>> GetErrors(this ModelStateDictionary modelState)
        {
            Dictionary<string, List<string>> allErors = new Dictionary<string, List<string>>();
            foreach (var prop in modelState.Where(ms => ms.Value.Errors.Any()))
            {
                List<string> errorMessages = new List<string>();
                foreach (var err in prop.Value.Errors)
                {
                    errorMessages.Add(err.ErrorMessage);
                }
                allErors.Add(prop.Key.Split('.')[1], errorMessages);
            }
            return allErors;
        }
    }

}